#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: gatoatigrado (nicholas tung) [ntung at ntung], Sagar Jain [sagarj at cs.berkeley.edu]
# Copyright 2010 University of California, Berkeley

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain a
# copy of the License at http://www.apache.org/licenses/LICENSE-2.0 .
from distutils.core import setup, Command
import distutils.command.build
from distutils.extension import Extension
from src.path_resolv import Path
from src.sketch_module import SketchModule, library_dirs

modpath = Path(__file__).parent()
sketchpath = modpath.parent().subpath("sketch-backend")
assert sketchpath.isdir(), "please check out sketch-backend in ../sketch-backend"

# Test Wrapper
#XXX: Need to create separate command for running example module
sketch_module = SketchModule("simple",
    [modpath.subpath("example/simple.cpp")]
    )

# Sketch Wrapper
sketchpaths = """
src/SketchSolver
src/SketchSolver/SolverInterfaces
src/SketchSolver/InputParser
src/abc60513/src/base/abc
src/abc60513/src/base/cmd
src/abc60513/src/base/main
src/abc61225/src/aig/hop
src/abc60513/src/sat/asat
src/abc60513/src/misc/vec
src/abc60513/src/misc/st
src/abc60513/src/misc/nm
src/abc60513/src/bdd/cudd
src/abc60513/src/misc/extra
src/abc60513/src/bdd/mtr
src/abc60513/src/bdd/epd
src/abc60513/src/misc/util
src/abc60513/src/base/io
"""
sketchpaths = [sketchpath.subpath(v) for v in sketchpaths.split("\n") if v]

def skfile(a):
    try:
        return (v for v in sketchpath.subpath("src/SketchSolver").walk_files()
            if v.basename() == a).next()
    except StopIteration:
        print("NOTE -- you may need to update the backend to the my-local branch")
        print("(issue \"hg up -C my-local\" in ../sketch-backend, and rebuild it)")
        print("file searching for: %s" %(a))
        exit(1)

sketch_module = SketchModule("_parseactions",
    map(skfile, """BooleanDAG.h
BooleanDAGCreator.h
BooleanNodes.h
CegisMain.h
InterpreterEnvironment.h""".split("\n")),
    include_paths=sketchpaths )


args = locals()
modules = [v for v in args.values() if isinstance(v, SketchModule)]

class GenBindings(Command):
    user_options=[("gccxml-path=", "g", "location of gccxml binary")]

    def initialize_options(self):
        self.gccxml_path = Path("/usr/bin/gccxml")

    def finalize_options(self):
        self.gccxml_path = Path(self.gccxml_path)
        assert self.gccxml_path.isfile(), "please specify the location of gccxml"

    def run(self):
        [v.build_bindings(self.gccxml_path) for v in modules]

# NOTE -- consider not requiring SKETCH installation
#libcegis = sketchpath.subpath("src/SketchSolver/.libs/libcegis.so")
#assert libcegis.exists(), "please build sketch-backend"
#Path("build").makedirs()
#libcegis.copy(Path("build/libcegis.so"))

setup(name="sketch_wrapper",
    cmdclass = { "gen_bindings": GenBindings },
    packages=["sketch_wrapper"],
    scripts=["scripts/cegis.py"],
    package_data={"sketch_wrapper": ["build/libcegis.so"]},
    ext_modules=[v.get_extension() for v in modules] )

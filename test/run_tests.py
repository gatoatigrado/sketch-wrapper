#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: gatoatigrado (nicholas tung) [ntung at ntung], Sagar Jain [sagarj at cs.berkeley.edu]
# Copyright 2010 University of California, Berkeley

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain a
# copy of the License at http://www.apache.org/licenses/LICENSE-2.0 .

import unittest
from test import test_support

class Test_FiniteDiff(unittest.TestCase):

    def test_example_one(self):
        import sys
        sys.path.insert(0, '../src/')
        import sketch_wrapper as SW
        args = (["", "-verbosity", "-1", "-overrideInputs", "3", "-assumebcheck", "Finite_Diff_Cases/set1.sk.tmp"])
        sketch = SW.Sketch(args)
        sketch.buildFuncDAGs()
        sketch.solveAll()

class Test_Array(unittest.TestCase):
    
    def test_example_one(self):
        import sys
        sys.path.insert(0, '../src/')
        import sketch_wrapper as SW
        args = (["", "-verbosity", "-1", "-overrideInputs", "3", "-assumebcheck", "Array_Cases/mas_Time.sk.tmp"])
        sketch = SW.Sketch(args)
        sketch.buildFuncDAGs()
        sketch.solveAll()
    
    def test_example_two(self):
        import sys
        sys.path.insert(0, '../src/')
        import sketch_wrapper as SW
        args = (["", "-verbosity", "-1", "-overrideInputs", "3", "-assumebcheck", "Array_Cases/mis_Time.sk.tmp"])
        sketch = SW.Sketch(args)
        sketch.buildFuncDAGs()
        sketch.solveAll()

    def test_example_three(self):
        import sys
        sys.path.insert(0, '../src/')
        import sketch_wrapper as SW
        args = (["", "-verbosity", "-1", "-overrideInputs", "3", "-assumebcheck", "Array_Cases/mss_Time.sk.tmp"])
        sketch = SW.Sketch(args)
        sketch.buildFuncDAGs()
        sketch.solveAll()

class Test_Minimization(unittest.TestCase):
    
    def test_example_one(self):
        import sys
        sys.path.insert(0, '../src/')
        import sketch_wrapper as SW
        args = (["", "-verbosity", "-1", "-overrideInputs", "3", "-assumebcheck", "Minimization_Cases/miniTest210MinRepeat.sk.tmp"])
        sketch = SW.Sketch(args)
        sketch.buildFuncDAGs()
        sketch.solveAll()
        sketch.minimizeSketch()
        assert sketch.envt.currentControls['H__2']==3

    def test_example_two(self):
        import sys
        sys.path.insert(0, '../src/')
        import sketch_wrapper as SW
        args = (["", "-verbosity", "-1", "-overrideInputs", "3", "-assumebcheck", "Minimization_Cases/sll.sk.tmp"])
        sketch = SW.Sketch(args)
        sketch.buildFuncDAGs()
        sketch.solveAll()
        sketch.minimizeSketch()
        assert sketch.envt.currentControls['H_100']==2

    def test_example_three(self):
        import sys
        sys.path.insert(0, '../src/')
        import sketch_wrapper as SW
        args = (["", "-verbosity", "-1", "-overrideInputs", "3", "-assumebcheck", "Minimization_Cases/tree.sk.tmp"])
        sketch = SW.Sketch(args)
        sketch.buildFuncDAGs()
        sketch.solveAll()
        sketch.minimizeSketch()
        assert sketch.envt.currentControls['H__27']==2

if __name__ == '__main__':
    unittest.main()

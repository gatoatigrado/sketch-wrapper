#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "animal.hpp"
using namespace std;

class Simple {
    public:
        int x;
        struct animal my_animal; 
        void printme() { cout << printinner() << endl; my_animal.m_name = "tiger"; cout << my_animal.name() << endl; }
        virtual int printinner() { return x; }
		animal* get_animal() { return &my_animal; }
		Simple* get_this() { return this; }
    private:
        int y;
};

void printSimple(Simple *s) { s->printme(); }

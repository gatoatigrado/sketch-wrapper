// Copyright 2004-2006 Roman Yakovenko.
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef __hello_world_hpp__
#define __hello_world_hpp__

#include <string>

enum color{ red, green, blue };

struct genealogical_tree{/*...*/};

struct animal{
    std::string m_name;
        
    explicit animal( const std::string& name="" )
    : m_name( name )
    {}

	genealogical_tree& genealogical_tree_ref()
    { return m_genealogical_tree; }

    std::string name() const
    { return m_name; }
   
private:    
    genealogical_tree m_genealogical_tree;
};

#endif//__hello_world_hpp__

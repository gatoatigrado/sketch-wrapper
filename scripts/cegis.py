#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: gatoatigrado (nicholas tung) [ntung at ntung]
# Copyright 2010 University of California, Berkeley

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain a
# copy of the License at http://www.apache.org/licenses/LICENSE-2.0 .
from __future__ import division, print_function
from collections import namedtuple, defaultdict
import sys; reload(sys); sys.setdefaultencoding('utf-8') # print unicode
from gatoatigrado_lib import Path

from sketch_wrapper.sketch import Sketch
import sys
import optparse

class CmdlineException(Exception): pass

def sketch_arg_conversions():
    class M(object):
        from copy import copy

        def __init__(self, lambda_, rewrite_):
            self.lambda_ = lambda_
            self.rewrite_ = rewrite_ 
            self.alt = []

        def __add__(self, other):
            assert isinstance(other, M)
            result = M.copy(self)
            result.alt.append(other)
            return result

        def eval(self, value):
            if self.lambda_(value):
                return self.rewrite_(value)
            for alt in self.alt:
                try: return alt.eval(value)
                except CmdlineException: pass
            raise CmdlineException("Don't know what to do with cmdline value '%s'" %(value))

        @classmethod
        def always(cls, rewrite_):
            return cls(lambda a: True, rewrite_)

    def const(arg): return lambda a: arg
    false = lambda a: not a


    # the variable names on the left are the long argument names
    # below, e.g. --bnd-inbits
    #--------------------------------------------------
    # sem_array_OOB_policy = (
    #     M("assertions".__eq__, const(["-assumebcheck"])) +
    #     M("wrsilent_rdzero".__eq__, const([])) )
    #-------------------------------------------------- 
    assumebcheck = M.always(lambda a: ["--assumebcheck"])
    bnd_inline_amnt = M.always(lambda a: ["--bnd-inline-amnt", str(a)])
    bnd_inbits = M.always(lambda a: ["--bnd-inbits", str(a)])
    bnd_int_range = M.always(lambda a: ["--bnd-int-range", str(a)])
    bnd_cbits = M.always(lambda a: ["--bnd-cbits", str(a)])
    seed = M.always(lambda a: ["--seed", str(a)])
    print_cex = M.always(lambda a: ["--print-cex"])
    olevel = M.always(lambda a: ["--olevel", str(a)])

    return ((k, v) for k, v in locals().items() if isinstance(v, M))

class ExtOptionGroup(optparse.OptionGroup):
    def __init__(self, group, *argv, **kwargs):
        optparse.OptionGroup.__init__(self, group, *argv, **kwargs)
        group.add_option_group(self)

    def intopt(self, z, *argv, **kwargs):
        self.add_option(*argv, default=z, type="int", metavar="Z", **kwargs)

    def maybe_intopt(self, *argv, **kwargs):
        self.add_option(*argv, type="int", metavar="Z", **kwargs)



if __name__ == '__main__':
    print("[cegis] got raw args", sys.argv, file=sys.stderr)
    cmdopts = optparse.OptionParser(usage="%prog [options] filename",
        description="Cegis scriptable backend for multiple solutions, and more",
        formatter=optparse.IndentedHelpFormatter(max_help_position=34, width=90)
        )

    cmdopts.add_option("-o", "--output-file",
        default="%(inpath)s/%(in)s-%(num)s.tmp",
        metavar="<fmt>",
        help="python format string for output name."
            r" e.g. '%(inpath)s/%(in)s-%(num)s.tmp'")
    cmdopts.add_option("-O", "--other-args", default="",
        metavar="<str>",
        help="Other arguments (space separated)."
            " TODO -- move into cegis.py and sketch.py")

    debug = ExtOptionGroup(cmdopts, "Debug",
        "Debugging options (a few may be platform-specific)")
    debug.add_option("-d", "--debug", action="store_true",
        help="set debug flag")
    debug.intopt(-4, "-v", "--verbosity",
        help="set the verbosity; default = -4")
    debug.add_option("--print-version", action="store_true",
        help="print version info")

    features = ExtOptionGroup(cmdopts, "Features",
        "Major features of the solver")
    features.intopt(1, "--num-solutions",
        help="number of alternate solutions to find; -1 = all")
    features.add_option("--use-minimize", action="store_true",
        help="minimize special holes in the sketch")
    features.intopt(4, "--min-iterations",
        help="number of iterations to calculate the minimum")
    features.maybe_intopt("--seed", help="pass this seed to the solver")
    features.add_option("--print-cex", action="store_true",
        help="show counterexamples as they are found by verification")
    features.maybe_intopt("--olevel", help="optimization level (1-5)")

    semantics = optparse.OptionGroup(cmdopts, "Semantics",
        "Alter semantics of the sketch (other than bounds)")
    semantics.add_option("--assumebcheck", action="store_true",
        help="Assume that array out-of-bound assertions have already been inserted. Otherwise, zeros are used.")
    #--------------------------------------------------
    # semantics.add_option("--sem-array-OOB-policy",
    #     default="assertions", metavar="_",
    #     help="Assert array indices are within bounds. 'assertions' or 'wrsilent_rdzero'.")
    #-------------------------------------------------- 
    cmdopts.add_option_group(semantics)

    bounds = ExtOptionGroup(cmdopts, "Bounds",
        "Set program finitization bounds")
    bounds.intopt(32, "--bnd-int-range", help="Range of all integers in program")
    bounds.intopt(3, "--bnd-inline-amnt", help="Level of inlining")
    bounds.intopt(5, "--bnd-inbits",
        help="The number of bits to use for integer inputs")
    bounds.intopt(5, "--bnd-cbits",
        help="The number of bits to use for integer holes")

    options, args = cmdopts.parse_args()
    if len(args) != 1:
        cmdopts.error("Need one argument -- input filename")

    # pass down any options this level isn't taking
    d = options.__dict__.copy()
    del d["num_solutions"], d["use_minimize"], d["other_args"]
    other_args = options.other_args.split(" ")
    other_args = [v.strip() for v in other_args if v]
    for k, v in sketch_arg_conversions():
        opt_value = getattr(options, k)
        if not opt_value is None:
            other_args.extend(v.eval(opt_value))
        del d[k]

    # print("[cegis] passing args to c backend:", other_args, file=sys.stderr)
    sketch = Sketch([__file__] + other_args + args, args[0], **d)

    sketch.buildFuncDAGs()
    if not sketch.solveAll():
        print ("no solution", file=sys.stderr)
        exit(1)

    if options.use_minimize:
        sketch.minimizeSketch()
    else:
        sketch.writeSolutions(0)
        sketch.getAlternateSolutions(options.num_solutions - 1)

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2009 gatoatigrado (nicholas tung) [ntung at ntung]
# from gatoatigrado_lib repository

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain a
# copy of the License at http://www.apache.org/licenses/LICENSE-2.0 .

from __future__ import print_function
from collections import namedtuple
import codecs, optparse, os, re, shutil, subprocess, types

home = os.path.expanduser(u"~")
assert os.path.isdir(home), "home directory must be present"

class Path(unicode):
    def __new__(cls, *unix_paths, **kwargs):
        args = []
        for path in unix_paths:
            if isinstance(path, str):
                path = path.decode("utf-8")
            if not path:
                raise Exception("no path", path)
            v = path[1:].split("/") # abs path handling
            v[0] = path[0] + v[0]
            args.extend(v)
        if args[0] == "~":
            args[0] = home
        elif args[0] == "!":
            assert False, "! is no longer supported. Try adding a global variable, " \
                "e.g. modpath = Path(__file__).parent()"
            import traceback
            stack = traceback.extract_stack()
            args[0] = os.path.dirname(os.path.realpath([v[0] for v in stack
                if not "gatoatigrado_lib" in v[0]][-1]))
        if kwargs.get("abspath", False) is True:
            path = os.path.abspath(os.path.join(*args))
        else:
            path = os.path.realpath(os.path.join(*args))
        return unicode.__new__(cls, unicode(path))
    
    def __str__(self):
        return unicode.encode(self, "utf-8")

    def __getattr__(self, key):
        if key in dir(os):
            fcn = getattr(os, key)
        elif key in dir(shutil):
            fcn = getattr(shutil, key)
        else:
            assert key in dir(os.path), key
            fcn = getattr(os.path, key)
        assert (type(fcn) == types.FunctionType or
            type(fcn) == types.BuiltinFunctionType)
        def inner(*argv, **kwargs):
            result = fcn(self, *argv, **kwargs)
            if type(result) == str:
                pathresult = Path(result, abspath=True)
                if result == str(pathresult):
                    return pathresult
            return result
        return inner

    def __getstate__(self): return None
    def __setstate__(self, state): assert not state



    # create Path objects from relative directories
    def subpath(self, *argv):
        return Path(self, *argv)

    def parent(self, nmore=0):
        result = self.dirname()
        for x in range(nmore):
            result = os.path.dirname(result)
        return Path(result, abspath=True)

    def chain(self):
        chain_ = [self]
        while chain_[0] != chain_[0].parent():
            chain_[0:0] = [chain_[0].parent()]
        return chain_

    def _stat_req(fcn):
        def _stat_req_inner(self, *argv, **kwargs):
            if not "_stat" in self.__dict__:
                self._stat = os.stat(self)
            return fcn(self, *argv, **kwargs)
        return _stat_req_inner

    @staticmethod
    def _get_datetime(value):
        assert type(value) == float
        import datetime
        return datetime.datetime.fromtimestamp(value)

    @_stat_req
    def created(self):
        return self._get_datetime(self._stat.st_ctime)

    @_stat_req
    def modified(self):
        return self._get_datetime(self._stat.st_mtime)

    @_stat_req
    def accessed(self):
        return self._get_datetime(self._stat.st_atime)



    # various file system manipulations
    def makedirs(self):
        if not self.exists():
            os.makedirs(self)
        assert self.isdir()
        return self

    def makenewdir(self):
        if self.exists():
            self.rmtree()
        self.makedirs()

    def remove_tree(self):
        if self.exists():
            self.rmtree() if self.isdir() else self.unlink()

    def copy(self, dst):
        dst = Path(dst) if not isinstance(dst, Path) else dst
        dst.parent().makedirs()
        shutil.copy(self, dst)

    def copyinto(self, dst):
        dst = Path(dst) if not isinstance(dst, Path) else dst
        if self.isfile():
            self.copy(dst)
        else:
            dst = dst.subpath(self.basename())
            dst.makedirs()
            [self.subpath(v).copyinto(dst) for v in self.listdir()]



    # override certain accessors to prevent relative path renaming
    def relpath(self, other):
        if other == "/":
            # not something crazy like "../name"
            # relpath should be the inverse of os.path.join
            return self[1:]
        return os.path.relpath(self, other)

    def basename(self):
        return os.path.basename(self)
    bn = basename



    # nicer accessors
    def subpaths(self):
        return (self.subpath(v) for v in self.listdir())

    def files(self):
        return (v for v in self.subpaths() if v.isfile())

    def dirs(self):
        return (v for v in self.subpaths() if v.isdir())

    def walk_all(self):
        for root, dirs, files in os.walk(self):
            root = Path(root, abspath=True)
            for d in dirs: yield root.subpath(d)
            for f in files: yield root.subpath(f)

    def walk_dirs(self):
        return (Path(root, d, abspath=True) for root, dirs, files in os.walk(self)
            for d in dirs)

    def walk_files(self, ext_list=None):
        g = (Path(root, f, abspath=True) for root, dirs, files in os.walk(self) for f in files)
        if ext_list:
            return (v for v in g if v.extension() in ext_list)
        else:
            return g

    # example: "/dir[name=src]/dir[name=grgen]/"
    def walk_xpathlike(self, xpath_like):
        return (v for v in self.walk_all() if v.matches(xpath_like))



    # i/o
    def openutf(self, mode="rb"):
        return codecs.open(self, mode, encoding="utf-8", errors="ignore")

    def read(self):
        return unicode(self.openutf().read())

    def lines(self):
        return [unicode(v).rstrip("\n") for v in self.openutf().readlines()]

    def grep(self, re):
        return (v for v in map(re.match, self.lines()) if v)

    def write(self, text):
        self.parent().makedirs()
        self.openutf("wb").write(unicode(text))

    def write_if_different(self, text):
        self.parent().makedirs()
        if (not self.isfile()) or (text != self.read()):
            self.write(text)
            return True

    def pickle(self, obj, xzcompresslevel=6):
        self.parent().makedirs()
        import cPickle, gzip, subproc
        if self.endswith(u".xz"):
            uncompr_path = Path(self.rstrip(u".xz"))
            if uncompr_path.isfile():
                uncompr_path.move(uncompr_path + ".backup")
            print("dumping to", uncompr_path)
            cPickle.dump(obj=obj, file=open(uncompr_path, "wb"), protocol=2)
            if self.isfile():
                self.move(self + ".backup")
            self.__class__.compress_cmds[self] = subproc.SubProc(
                ["xz", "-%d" %(xzcompresslevel), self.rstrip(u".xz")])
        else:
            cPickle.dump(obj=obj, file=gzip.open(self, "wb", compresslevel=3), protocol=2)

    # path -> cmd
    compress_cmds = {}

    @classmethod
    def compress_pickled(cls):
        [v.start_wait() for v in cls.compress_cmds.values()]

    def unpickle(self):
        import cPickle, gzip, subproc
        if self.endswith(u".xz"):
            uncompr_path = Path(self.rstrip(u".xz"))
            if uncompr_path.isfile():
                uncompr_path.move(uncompr_path + ".backup")
            try:
                subproc.SubProc(["xz", "-d", "-k", self]).start_wait()
            except Exception, e:
                try: uncompr_path.unlink()
                except: pass
                raise e
            result = cPickle.load(open(uncompr_path, "rb"))
            uncompr_path.unlink()
            return result
        else:
            return cPickle.load(gzip.open(self, "rb"))

    def md5sum(self):
        from hashlib import md5
        return md5(self.read()).hexdigest()



    # "png", etc.
    def extension(self):
        return self.splitext()[1][1:]

    def __repr__(self): return "Path[\"%s\"]" % (str(self))

    @classmethod
    def pathjoin(cls, *argv):
        assert all([type(v) != list for v in argv]), "pathjoin takes varargs"
        return os.path.pathsep.join([str(v) for v in argv])

    # export to global visibility
    @classmethod
    def resolve(cls, *paths):
        for path in paths:
            try:
                return resolve_single(path)
            except: pass
        raise Exception("can't resolve paths '%s'" % (path))

import atexit
atexit.register(Path.compress_pickled)

class PathResolver:
    def get_file(self, basename):
        raise NotImplementedError("abstract")

class DirArrayPathResolver:
    def __init__(self, array):
        self.dir_arr = []
        self.add_paths(array)

    def add_paths(self, array):
        for dirname in array:
            if dirname:
                path = Path(dirname)
                if path.exists() and not path in self.dir_arr:
                        self.dir_arr.append(path)

    def get_resolve_list(self, basename):
        subpaths = [dir_.subpath(basename) for dir_ in self.dir_arr]
        return [subpath for subpath in subpaths if subpath.exists()]

class EnvironPathResolver(DirArrayPathResolver):
    def __init__(self):
        DirArrayPathResolver.__init__(self, [])
        environ = os.environ.items()
        # if key is priority, then sort key is zero, else one.
        environ.sort(key=lambda k: k[0] not in ["PATH"])
        [self.add_paths(v.split(os.path.pathsep)) for k, v in environ]

# example
#class DefaultPathResolver(DirArrayPathResolver):
    #def __init__(self):
        #DirArrayPathResolver.__init__(self, [".", "~/sandbox/eclipse"])

resolvers = [EnvironPathResolver()]

def resolve_single(path):
    results = []
    for resolver in resolvers:
        results.extend(resolver.get_resolve_list(path))
    results = [path for i, path in enumerate(results) if not path in results[:i]]
    if not results:
        raise Exception("can't resolve path '%s'" % (path))
    elif len(results) > 1:
        from warnings import warn
        warn("multiple resolutions for '%s':" % (path), results)
    return results[0]

class ExecuteIn:
    def __init__(self, path):
        self.prev_path = Path(".")
        self.path = path

    def __enter__(self):
        self.path.chdir()

    def __exit__(self, type_, value, tb):
        self.prev_path.chdir()

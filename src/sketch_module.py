#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: gatoatigrado (nicholas tung) [ntung at ntung], Sagar Jain [sagarj at cs.berkeley.edu]
# Copyright 2010 University of California, Berkeley

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain a
# copy of the License at http://www.apache.org/licenses/LICENSE-2.0 .
from __future__ import division, print_function
from collections import namedtuple, defaultdict
from distutils.extension import Extension
from path_resolv import Path
import sys
import re
from pyplusplus import module_builder

modpath = Path(__file__).parent()
sketchpath = modpath.parent(1).subpath("sketch-backend")
assert sketchpath.isdir(), "please check out sketch-backend in ../sketch-backend"
sketchlib = sketchpath.subpath("src/SketchSolver/.libs")
assert sketchlib.isdir(), "please build the sketch-backend in ../sketch-backend"

if sys.platform == "win32" :
    include_dirs = ["C:/Boost/include/boost","."]
    libraries=["boost_python-mgw"]
    library_dirs=["C:/Boost/lib"]
elif sys.platform == "darwin" :
    include_dirs = ["/opt/local/include", "/opt/local/include/boost", "."]
    libraries=["boost_python", "cegis"]
    library_dirs = ["/usr/lib", "/opt/local/lib", sketchlib]
else:
    include_dirs = ["/usr/include/boost", "."]
    libraries=["boost_python", "cegis"]
    library_dirs=["/usr/lib", "/usr/lib64", "/usr/local/lib", sketchlib]

#map of class to list of its functions to be exported
class_func_dict = dict()

#list of classes to be exported
classes_to_export = [
    "InterpreterEnvironment",
    "CommandLineArgs",
    "SATSolver",
    "BooleanDAG",
    "bool_node",
    "CONST_node",
    "CTRL_node"
]

#populating the dictionary of class:list of functions to be exported
class_func_dict["BooleanDAG"] = [
    "getIntSize",
    "getNodesByType",
    "print_wrapper",
    "lprint_wrapper",
    "new_node",
    "repOK",
    "addNewNode",
    "clone",
    # "[]",
    #"getNodes"
]
class_func_dict["InterpreterEnvironment"] = [
    "getCopy",
    "set_function",
    "prepareMiter",
    "assertDAG_wrapper",
    "printControls_wrapper"
]
class_func_dict["CommandLineArgs"] = [
    "setPARAMS"
]
class_func_dict["bool_node"] = [
    "clone"
]
class_func_dict["CONST_node"] = [
    "getVal",
    "setVal"
]
class_func_dict["CTRL_node"] = [
    "get_name",
    "get_toMinimize"
]

class Renames:
    class_fcns = {
        "BooleanDAG" : {
            "size" : "__len__"
            },
        "bool_node" : {
            "lprint" : "__repr__"
            }
        }

class CallPolicies:
    from pyplusplus.module_builder import call_policies
    managedReturn = call_policies.return_value_policy(call_policies.manage_new_object)
    pointerReturn = call_policies.return_value_policy(call_policies.return_pointee_value)
    copyRefReturn = call_policies.return_value_policy(call_policies.copy_non_const_reference)
    existingRefReturn = call_policies.return_value_policy(call_policies.reference_existing_object)

    class_policies = {
        "InterpreterEnvironment" : {
            "getCopy" : managedReturn,
            "prepareMiter" : pointerReturn
            },
        "BooleanDAG" : {
            "clone" : managedReturn,
            "new_node" : existingRefReturn,
            "getNodesByType" : copyRefReturn,
            #"getNodes" : existingRefReturn
            },
        "bool_node" : {
            "clone" : managedReturn
            }
        }

    free_funcs_policies = {
        "getEnvt" : pointerReturn
        }


#list of free-functions to be exported
free_funcs_to_export = [
    "runDriver",
    "getEnvt"
]

#list of global variables to exclude 
glob_vars_to_ignore = [
    "solution",
    "modelBuilding",
    "assertions",
    "yyin",
    "yylineno",
    "yytext",
    "yy_flex_debug",
    "envt",
    "PARAMS"
]

# check for presence of global vars
glob_vars_to_check = [
    "VERSION_INFO"
]

#list of classes whose constructors are to be exported
desired_cons_of = [
    "CommandLineArgs",
    "InterpreterEnvironment",
    "CONST_node",
    "BooleanDAG"
]

class SketchModule(object):
    def __init__(self, modname, sourcefiles, include_paths=[]):
        self.modname, self.sourcefiles = modname, sourcefiles
        self.bindings_file = modpath.subpath(self.modname + "-binding.cpp")
        self.include_dirs=include_dirs + map(unicode, include_paths)

    def inc_constructors(self, mb):
        """ Includes the desired constructors and deconstructors"""
        for call in mb.calldefs():
            if call.parent.name in desired_cons_of:
                if call.name == call.parent.name or call.name == '~' + call.parent.name:
                    call.include()

    def build_bindings(self, gccxml_path):
        from pyplusplus import function_transformers as FT
        from pygccxml.declarations.cpptypes import reference_t
        from pygccxml.parser.declarations_cache import file_cache_t

        sys.setrecursionlimit(10000)

        #Creating the module_builder_t instance
        Path("~/.sketch/tmp").makedirs()
        mb = module_builder.module_builder_t( map(unicode, self.sourcefiles)
            , gccxml_path=unicode(gccxml_path)
            , cache=file_cache_t(Path("~/.sketch/tmp/py++_cache"))
            , working_directory=unicode(modpath)
            , include_paths=self.include_dirs
            , define_symbols=[] )

        #Excluding all classes
        mb.classes().always_expose_using_scope = True
        mb.classes().exclude()
        
        #Including the desired classes
        map(lambda v: mb.class_(v).include(), classes_to_export)

        #Excluding all methods
        mb.calldefs().exclude()

        #Including constructors of desired classes
        self.inc_constructors(mb);

        #You may not want to include all the constructors. Handle constructors to exclude here.
        cmdLineArgs_cons = mb.constructors('CommandLineArgs')
        map(lambda v: v.exclude(), filter(lambda v: v.decl_string == ' ( ::CommandLineArgs::* )( int,char * * ) ', cmdLineArgs_cons))

        #Handle class modifications here
        booleanDAG = mb.class_('BooleanDAG')
        booleanDAG.held_type = 'std::auto_ptr< %s >' % booleanDAG.decl_string
        CTRL_node = mb.class_('CTRL_node')
        bool_node = mb.class_('bool_node')
        CTRL_node.held_type = 'bp::bases< %s >' % bool_node.decl_string
        
        #Including the desired free-functions
        map(lambda v: mb.free_function(v).include(), free_funcs_to_export)
        
        #Including the desired member-functions
		#TODO: Insert the correct error-message
        map(lambda v: [mb.class_(v).mem_funs(u).include() for u in class_func_dict.get(v)], class_func_dict.keys())
        for cls, rules in Renames.class_fcns.items():
            cls = mb.class_(cls)
            [cls.mem_funs(k).include() for k in rules]
            [cls.mem_funs(k).rename(v) for k, v in rules.items()]

        #Excluding few global variables
        map(lambda v: mb.var(v).exclude(), glob_vars_to_ignore)
        for v in glob_vars_to_check:
            try: mb.var(v)
            except:
                raise Exception("can't find required var '%s'" %(v))
        
        # include operator [] for booleanDag
        # booleanDAG.operators("[]")[0].include()



        # === Handle call_policies here
        for fcn, rule in CallPolicies.free_funcs_policies.items():
            mb.free_function(fcn).call_policies = rule
        for cls, rules in CallPolicies.class_policies.items():
            for fcn, rule in rules.items():
                mb.class_(cls).mem_fun(fcn).call_policies = rule

        mb.class_('InterpreterEnvironment').mem_fun('prepareMiter').add_transformation(
            FT.transfer_ownership(0), FT.transfer_ownership(1))
        mb.class_('InterpreterEnvironment').mem_funs('assertDAG_wrapper').add_transformation(FT.transfer_ownership(0))



        # === Write out the file
        mb.build_code_creator( module_name=self.modname )
        mb.code_creator.license = """// author: gatoatigrado (nicholas tung) [ntung at ntung], Sagar Jain [sagarj at cs.berkeley.edu]
// Copyright 2010 University of California, Berkeley

// Licensed under the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License. You may obtain a
// copy of the License at http://www.apache.org/licenses/LICENSE-2.0 ."""
        mb.write_module( unicode(self.bindings_file) )
        text = self.bindings_file.read()

        # replace absolute paths with relative paths
        absolute, relative = modpath, ""
        while len(absolute) > 5:
            if not relative:
                text = text.replace(absolute + "/", relative)
            text = text.replace(absolute, relative)
            absolute = absolute.parent()
            relative = ".." + ("/" if relative else "") + relative

        # making explicitly 'BooleanDAG' class noncopyable=False
        text = re.sub("typedef bp::class_< BooleanDAG, std::auto_ptr< ::BooleanDAG >, boost::noncopyable > BooleanDAG_exposer_t;", "typedef bp::class_< BooleanDAG, std::auto_ptr< ::BooleanDAG > > BooleanDAG_exposer_t;", text)
        
        # handling ugly aliasing explicitly
        assertDAG_ugly_aliases = re.findall(re.compile(r'(assertDAG_wrapper_[a-z0-9]+)'), text)
        for ugly_alias in assertDAG_ugly_aliases:
            text = re.sub(ugly_alias, 'assertDAG_wrapper', text)
        self.bindings_file.write(text)

    def get_extension(self):
        return Extension(self.modname, [str(self.bindings_file)],
            library_dirs=library_dirs, libraries=libraries,
            include_dirs=self.include_dirs,
            runtime_library_dirs=library_dirs,
            depends=[])

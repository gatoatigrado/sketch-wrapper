SHELL = /bin/bash

help:
	@echo "NOTE - this makefile is mostly unix aliases. Use 'mvn install' to build."
	@grep -iE "^(###.+|[a-zA-Z0-9_-]+:.*(#.+)?)$$" Makefile | sed -u -r "/ #HIDDEN/d; s/^### /\n/g; s/:.+#/#/g; s/^/    /g; s/#/\\n        /g; s/:$$//g"

gatoatigrado_lib:
	echo "run (aptitude|zypper|your-package-manager) install \"python-distribute\""
	sudo easy_install "https://bitbucket.org/gatoatigrado/gatoatigrado_lib/get/default.tar.bz2"

gccxml: PHONY
	[ -d gccxml ] || git clone git://github.com/gatoatigrado/gccxmlclone.git gccxml
	mkdir -p gccxml-build; (cd gccxml-build; cmake ../gccxml -DCMAKE_INSTALL_PREFIX:PATH=/usr && make -j8 && sudo make install)

pygccxml: PHONY
	[ -d pyplusplus ] || git clone git://github.com/gatoatigrado/pyplusplusclone.git pyplusplus
	(cd pyplusplus/pygccxml_dev; python setup.py build && sudo python setup.py install)
	(cd pyplusplus/pyplusplus_dev; python setup.py build && sudo python setup.py install)

clean:
	rm -rf build

build: PHONY
	python setup.py clean gen_bindings build
	sudo python setup.py install

test: PHONY
	cd test
	python run_tests.py

PHONY:

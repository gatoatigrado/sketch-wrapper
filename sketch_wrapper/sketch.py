#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: gatoatigrado (nicholas tung) [ntung at ntung], Sagar Jain [sagarj at cs.berkeley.edu]
# Copyright 2010 University of California, Berkeley

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain a
# copy of the License at http://www.apache.org/licenses/LICENSE-2.0 .
from parseactions import *
from iterator_wrapper import EnumIterator

from shutil import copy, move
from math import ceil
import sys
import re

from collections import namedtuple, defaultdict
import sys; reload(sys); sys.setdefaultencoding('utf-8') # print unicode
from gatoatigrado_lib import Path
import itertools

from rlimits import set_rlimits
set_rlimits()

class Sketch:
    def __init__(self, args, infile, debug, verbosity,
            output_file, min_iterations, print_version):
        self.cmdLineArgs = CommandLineArgs(args)

        self.infile = Path(infile)
        assert self.infile.isfile(), "input '%s' doesn't exist" %(infile)
        self.inpath = self.infile.parent()
        self.in_basename = self.infile.basename()
        self.debug = debug
        self.min_iterations = min_iterations
        self.output_file = output_file

        self.cmdLineArgs.verbosity = verbosity
        self.cmdLineArgs.debug = bool(debug)
        self.cmdLineArgs.setPARAMS()
        self.sketch_spec = []
        self.envt = None
        self.bDag = None

        if print_version:
            print("CEGIS-scripting version features: %s, %s" %(
                VERSION_INFO, "minimization, mult-soln"))

    def buildFuncDAGs(self):
        '''Builds the BooleanDAGs corresponding to all the functions present in the input file'''
        inpFile = open(self.cmdLineArgs.inputFname, 'r')
        identifier = re.compile(r'([a-zA-Z_\#][a-zA-Z_0-9]*)')
        sketchSentence = "(assert %s SKETCHES %s;)" % (identifier.pattern, identifier.pattern)
        text = inpFile.read()
        inpFile.close()
        copy(self.cmdLineArgs.inputFname, self.cmdLineArgs.inputFname + '.bak')
        outFile = open(self.cmdLineArgs.inputFname, 'w')
        sketch_lines = re.findall(sketchSentence, text)
        map(lambda (_, sketch, spec): self.sketch_spec.append((sketch, spec)), sketch_lines)
        for line in sketch_lines:
            text = re.sub(line[0], "", text)
        outFile.write(text)
        outFile.flush()
        outFile.close()
        runDriver()
        self.envt = getEnvt()
        move(self.cmdLineArgs.inputFname + '.bak', self.cmdLineArgs.inputFname)

    def addSketchSpecPair(self, sketch, spec):
        '''Creates the BooleanDAG to be asserted for "sketch" to implement "spec"'''
        dag = self.envt.prepareMiter(self.envt.getCopy(spec), self.envt.getCopy(sketch))
        self.bDag = dag.clone()
        return dag

    def resolveDag(self, dag):
        '''Resolves the input BooleanDAG'''
        # self.setRandomInputs(dag)
        self.envt.assertDAG_wrapper(dag)

    def setRandomInputs(self, dag):
        newDag = BooleanDAG()
        bgProblem = dag.clone()
        inputNodes = bgProblem.getNodesByType(bool_node.SRC)
        for inputNode in EnumIterator(inputNodes):
            print("input node", inputNode)

    def solveAll(self):
        '''Default method to solve the sketch'''
        # print >> sys.stdout, "[cegis] solving..."
        map(lambda dag: self.resolveDag(dag),
            map(lambda (sketch, spec): self.addSketchSpecPair(sketch, spec), self.sketch_spec))
        # print >> sys.stdout, "[cegis] done"
        return self.envt.status == InterpreterEnvironment.STATUS.READY

    def writeSolutions(self, num):
#        d = self.__dict__
#        d.update(locals())
#        d["in"] = self.in_basename # python keyword
        outname = Path(self.output_file + str(num))
        outname.parent().makedirs()
        outname = str(outname)
        print >> sys.stdout, "[cegis] writing output to '%s'" %(outname)
        self.envt.printControls_wrapper(outname)

    def getAlternateSolutions(self, num=-1):
        '''Finds the alternate solutions with respect to the current dag in 'self.bDag'. Input 'num' determines the number of alternate solutions to find, by default this method tries to find all the possible alternate solutions. '''
        self.cmdLineArgs.verbosity = -1
        self.cmdLineArgs.setPARAMS()
        for cnt in (xrange(1, num + 1) if num >= 0 else itertools.count(0)):
            if not self.__getAnotherSolution():
                break
            self.writeSolutions(num=cnt)

    def __getAnotherSolution(self):
        '''Returns true if a different satisfiable solution exists from the input 'dag' otherwise returns false. Treat this function as private, as it modifies the 'self.bDag' object. Use getAlternateSOlutions() method.'''
        self.cmdLineArgs.verbosity = -1
        self.cmdLineArgs.setPARAMS()
        newDag = BooleanDAG()
        bgProblem = self.bDag.clone()
        ctrlNodes = bgProblem.getNodesByType(bool_node.CTRL)
        lNotCtrlNodes = []
        toAssert = []
        for i in xrange(len(ctrlNodes)):
            constNode = newDag.new_node(None, None, bool_node.CONST)
            constNode.setVal(self.envt.currentControls[ctrlNodes[i].get_name()])
            newDag.addNewNode(ctrlNodes[i])
            eqNode = newDag.new_node(ctrlNodes[i], constNode, bool_node.EQ)
            lNotCtrlNodes.append(newDag.new_node(eqNode, None, bool_node.NOT))
        if len(lNotCtrlNodes) == 1:
            toAssert.append(lNotCtrlNodes[i])
        elif len(lNotCtrlNodes) == 0:
            return False
        else:
            toAssert.append(reduce(lambda v, u: newDag.new_node(v, u, bool_node.OR), lNotCtrlNodes))
        for i in xrange(len(toAssert)):
            newDag.new_node(toAssert[i], None, bool_node.ASSERT)
        self.envt.assertDAG_wrapper(newDag)
        if self.envt.status == InterpreterEnvironment.STATUS.READY:
            return True
        else:
            return False

    def resetEnvt(self):
        '''Resets the InterpreterEnvironment object'''
        self.envt.status = InterpreterEnvironment.STATUS.READY
        self.envt.bgproblem = None

    def minimizeSketch(self):
        '''Minimizes the sketch'''
        assert self.envt.status == InterpreterEnvironment.STATUS.READY, "Can't minimize the buggy sketch"
        self.cmdLineArgs.verbosity = -1
        self.cmdLineArgs.setPARAMS()
        ctrlNodes = self.bDag.getNodesByType(bool_node.CTRL)
        toMinimizeNodes = []
        for i in xrange(len(ctrlNodes)):
            if ctrlNodes[i].get_toMinimize():
                toMinimizeNodes.append(ctrlNodes[i])
        for i in xrange(len(toMinimizeNodes)):
            self.minimizeHole(toMinimizeNodes[i])

    def minimizeHole(self, hole):
        '''Minimizes the input hole'''
        print('***** Minimizing hole ' + hole.get_name() + ' *****')
        toIterate = True
        lastSuccessfulValue = self.envt.currentControls[hole.get_name()]
        while(toIterate):
            self.writeSolutions(num=0)
            print 'lastSuccessfulValue = ', lastSuccessfulValue
            newDag = BooleanDAG()
            bgProblem = self.bDag.clone()
            ctrlNodes = bgProblem.getNodesByType(bool_node.CTRL)
            hole_clone = None
            for i in xrange(len(ctrlNodes)):
                if(ctrlNodes[i].get_name() == hole.get_name()):
                    hole_clone = ctrlNodes[i]
                    break
            newDag.addNewNode(hole_clone)
            constNode = newDag.new_node(None, None, bool_node.CONST)
            constNode.setVal(lastSuccessfulValue)
            ltNode = newDag.new_node(hole_clone, constNode, bool_node.LT)
            assertNode = newDag.new_node(ltNode, None, bool_node.ASSERT)
            self.envt.assertDAG_wrapper(newDag)
            if self.envt.status == InterpreterEnvironment.STATUS.READY:
                lastSuccessfulValue = self.envt.currentControls[hole.get_name()]
            elif self.envt.status == InterpreterEnvironment.STATUS.UNSAT:
                toIterate = False
            else:
                assert False, "Missing case for InterpreterEnvironment.STATUS enum"     
        constNode = self.bDag.new_node(None, None, bool_node.CONST)
        constNode.setVal(lastSuccessfulValue)
        eqNode = self.bDag.new_node(hole, constNode, bool_node.EQ)
        self.bDag.new_node(eqNode, None, bool_node.ASSERT)
        print('Minimum value of hole ' + str(hole.get_name()) + ' is ' + str(lastSuccessfulValue))


#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: gatoatigrado (nicholas tung) [ntung at ntung], Sagar Jain [sagarj at cs.berkeley.edu]
# Copyright 2010 University of California, Berkeley

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain a
# copy of the License at http://www.apache.org/licenses/LICENSE-2.0 .
from _parseactions import *

# Fixing the constructor for CommandLineArgs class
oldinit = CommandLineArgs.__init__
def override(self, lst):
    vec = vector_less__std_scope_string__greater_()
    map(vec.append, lst)
    oldinit(self, vec)
CommandLineArgs.__init__ = override

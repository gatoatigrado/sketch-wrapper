#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: Sagar Jain [sagarj at cs.berkeley.edu]
# Copyright 2010 University of California, Berkeley

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain a
# copy of the License at http://www.apache.org/licenses/LICENSE-2.0 .

import sys
import os.path
import datetime
import random
import re

class Runner:
    '''This class is intended to run cegis experiments'''
    def __init__(self, cegisPath, benchmarkPath, outFile, cexFile, timeout):
        os.path.isfile(cegisPath)
        self.cegisPath = cegisPath    #Path to cegis binary
        os.path.isfile(benchmarkPath)
        self.benchmarkPath = benchmarkPath #Path to benchmark file
        if cexFile != "":
            os.path.isfile(cexFile)
        self.cexFile = cexFile #Path to file containing the list of counter-examples
        self.timeout = timeout #Solver is killed after given number of seconds
        self.outFile = outFile #Path to file on which logging needs to be done
        paramFH = open('random-params.config', 'r')
        self.parameters = paramFH.readlines()
        paramFH.close()
        self.parameters = map(lambda x: x.split('\n')[0], self.parameters)

    def evaluateBenchmark(self, params):
        '''Evaluates the benchmark with the input set of params'''
        decimalNumber = re.compile(r'(\d+\.?\d*)')
        pathToTestCase = self.benchmarkPath
        #Preparing command to send to timeout_command
        cmd = []
        cmd.append('./timeout.sh')
        cmd.append('-t')
        cmd.append(self.timeout)
        cmd.append(self.cegisPath)
        for item in params:
            cmd.append(item)
        cmd.append('-verbosity')
        cmd.append('1')
        if self.cexFile != "":
            cmd.append('-useFixedInputs')
            cmd.append(self.cexFile)
        cmd.append(pathToTestCase)
        import subprocess
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        result = process.wait()
        text = process.stdout.read()
        detailTimeList = re.findall(r'\*\*\*\*\*\*\*\*  (\d+)\tftime= (\d+\.?\d)\tctime= (\d+\.?\d+)', text)
        if len(detailTimeList) != 0:
            detailTimeList = map(lambda x: x[0] + ' ' + x[1] + ' ' + x[2], detailTimeList)
        detailTimeString = ''
        for iterTime in detailTimeList:
            detailTimeString += iterTime + ' '
        seedList = re.findall(r'SOLVER RAND SEED = (\d+)', text)
        seed = 'None'
        if len(seedList) != 0:
            seed = seedList[0]
        timeList = re.findall(r'TOTAL TIME (\d+\.?\d+)', text)
        time = 'None'
        if len(timeList) != 0:
            time = timeList[0]
        outDetailFileHandle = open(self.outFile + '.detail', 'a')
        outSeedFileHandle = open(self.outFile + '.seed', 'a')
        outDetailFileHandle.write(str(params) + " " + detailTimeString + "\n")
        outSeedFileHandle.write(str(params) + " " + seed + "\n")
        outDetailFileHandle.flush()
        outDetailFileHandle.close()
        outSeedFileHandle.flush()
        outSeedFileHandle.close()
        if result == 0:
            outFileHandle = open(self.outFile, 'a')
            outFileHandle.write(str(params) + " " + str(time) + "\n")
            outFileHandle.flush()
            outFileHandle.close()
        else:
            outFileHandle = open(self.outFile, 'a')
            outFileHandle.write(str(params) + " None\n")
            outFileHandle.flush()
            outFileHandle.close()

    def randomWalk(self, walkType):
        '''At each iteration the method randomly picks the parameter and log the run information'''
        #TODO: Introduce enum for walkType
        progress = 0
        for params in self.parameters:
            paramList = []
            print 'progress = ', str(progress)
            progress += 1
            if walkType == 0:
                paramList.append('-synthParams')
                paramList.append(params)
            elif walkType == 1:
                paramList.append('-verifParams')
                paramList.append(params)
            self.evaluateBenchmark(paramList) 
            
if __name__ == '__main__':
    #TODO: Handling of sys arguments
    loadRunner = Runner(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    loadRunner.randomWalk(0)

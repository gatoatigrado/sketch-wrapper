import sys
import os.path
import numpy
import re

if __name__ == '__main__':
    os.path.isfile(sys.argv[1])
    inpLog = open(sys.argv[1], 'r')
    detailLog = open(sys.argv[2], 'r')
    log = inpLog.readlines() 
    detail_log = detailLog.readlines() 
    inpLog.close()
    detailLog.close()
    params_dict = {}
    line_num = 0
    for line in log:
        beginParam = line.find('[')
        endParam = line.find(']')
        param = line[beginParam+1:endParam]
        if line[endParam+1:].strip() == 'None':
            time = -1
        else:
            detail_line = detail_log[line_num]
            det_end_param = detail_line.find(']')
            if detail_line[det_end_param+1:].strip() == '':
                time = -1
            else:
                time = float(line[endParam+1:])*10**-03
        if params_dict.has_key(param):
            oldList = params_dict[param]
            oldList.append(time)
            params_dict[param] = oldList
        else:
            params_dict[param] = [time] 
        line_num += 1
    #Summarize
    decimalNumber = re.compile(r'(-?\d+\.?\d*)')
    params_opt_dict = {}
    for k, v in params_dict.iteritems():
        l = filter(lambda x: x != -1, v)
        mean = 2700 - numpy.mean(l)
        stdev = numpy.std(l)
        if len(v) != 0 and len(l) == 0:
            params_opt_dict[k] = -2700
        else:
            params_opt_dict[k] = [mean, stdev, 0]
        if -1 in v:
            params_dict[k] = [-2700, stdev, -1]
        else:
            params_dict[k] = [mean, stdev, 0]
    histo = numpy.zeros((4, 4, 4))
    for k, v in params_dict.iteritems():
        params = re.findall(decimalNumber, k)
        stats = re.findall(decimalNumber, str(v))
        mean = stats[0]
        histo[min(3, float(params[0])*10 - 8), min(float(params[1])*10 - 8, 3), min(3, float(params[2])*10)] = float(mean)
    datFH = open(sys.argv[1] + '.dat', 'w')
    for x in xrange(4):
        for y in xrange(4):
            for z in xrange(4):
                datFH.write(str(x) + ' ' + str(y) + ' ' + str(z) + ' ' + str(histo[x][y][z]) + '\n')
            datFH.write('\n')
    datFH.flush()
    datFH.close() 
    histo_opt = numpy.zeros((4, 4, 4))
    for k, v in params_opt_dict.iteritems():
        params_opt = re.findall(decimalNumber, k)
        stats = re.findall(decimalNumber, str(v))
        mean = stats[0]
        histo_opt[min(3, float(params_opt[0])*10 - 8), min(float(params_opt[1])*10 - 8, 3), min(3, float(params_opt[2])*10)] = float(mean)
    datFH = open(sys.argv[1] + '.opt.dat', 'w')
    for x in xrange(4):
        for y in xrange(4):
            for z in xrange(4):
                datFH.write(str(x) + ' ' + str(y) + ' ' + str(z) + ' ' + str(histo_opt[x][y][z]) + '\n')
            datFH.write('\n')
    datFH.flush()
    datFH.close() 
    datFH1 = open(sys.argv[1] + '.dat.1', 'w')
    for x in xrange(4):
        for y in xrange(4):
            slice_data = histo[x][y][0:4]
            if -2700 in slice_data:
                datFH1.write(str(x) + ' ' + str(y) + ' -2700\n')
            else:
                l = filter(lambda x: x != 0, slice_data)
                datFH1.write(str(x) + ' ' + str(y) + ' ' + str(numpy.mean(l)) + '\n')
        datFH1.write('\n')
    datFH1.flush()
    datFH1.close() 
    datFH2 = open(sys.argv[1] + '.dat.2', 'w')
    for y in xrange(4):
        for z in xrange(4):
            slice_data = [histo[0][y][z], histo[1][y][z], histo[2][y][z], histo[3][y][z]]
            if -2700 in slice_data:
                datFH2.write(str(y) + ' ' + str(z) + ' -2700\n')
            else:
                l = filter(lambda x: x != 0, slice_data)
                datFH2.write(str(y) + ' ' + str(z) + ' ' + str(numpy.mean(l)) + '\n')
        datFH2.write('\n')
    datFH2.flush()
    datFH2.close() 
    datFH3 = open(sys.argv[1] + '.dat.3', 'w')
    for x in xrange(4):
        for z in xrange(4):
            slice_data = [histo[x][0][z], histo[x][1][z], histo[x][2][z], histo[x][3][z]]
            if -2700 in slice_data:
                datFH3.write(str(x) + ' ' + str(z) + ' -2700\n')
            else:
                l = filter(lambda x: x != 0, slice_data)
                datFH3.write(str(x) + ' ' + str(z) + ' ' + str(numpy.mean(l)) + '\n')
        datFH3.write('\n')
    datFH3.flush()
    datFH3.close() 
    datFH1 = open(sys.argv[1] + '.opt.dat.1', 'w')
    for x in xrange(4):
        for y in xrange(4):
            slice_data = histo_opt[x][y][0:4]
            filt_unterm = filter(lambda x: x != -2700, slice_data)
            if len(filt_unterm) == 0:
                datFH1.write(str(x) + ' ' + str(y) + ' -2700\n')
            else:
                l = filter(lambda x: x != 0, filt_unterm)
                datFH1.write(str(x) + ' ' + str(y) + ' ' + str(numpy.mean(l)) + '\n')
        datFH1.write('\n')
    datFH1.flush()
    datFH1.close() 
    datFH2 = open(sys.argv[1] + '.opt.dat.2', 'w')
    for y in xrange(4):
        for z in xrange(4):
            slice_data = [histo_opt[0][y][z], histo_opt[1][y][z], histo_opt[2][y][z], histo_opt[3][y][z]]
            filt_unterm = filter(lambda x: x != -2700, slice_data)
            if -2700 in slice_data:
                datFH2.write(str(y) + ' ' + str(z) + ' -2700\n')
            else:
                l = filter(lambda x: x != 0, filt_unterm)
                datFH2.write(str(y) + ' ' + str(z) + ' ' + str(numpy.mean(l)) + '\n')
        datFH2.write('\n')
    datFH2.flush()
    datFH2.close() 
    datFH3 = open(sys.argv[1] + '.opt.dat.3', 'w')
    for x in xrange(4):
        for z in xrange(4):
            slice_data = [histo_opt[x][0][z], histo_opt[x][1][z], histo_opt[x][2][z], histo_opt[x][3][z]]
            filt_unterm = filter(lambda x: x != -2700, slice_data)
            if -2700 in slice_data:
                datFH3.write(str(x) + ' ' + str(z) + ' -2700\n')
            else:
                l = filter(lambda x: x != 0, filt_unterm)
                datFH3.write(str(x) + ' ' + str(z) + ' ' + str(numpy.mean(l)) + '\n')
        datFH3.write('\n')
    datFH3.flush()
    datFH3.close() 
    outPlotFile = open(sys.argv[1] + '.plot.1', 'w')
    outPlotFile.write('set out \'' + sys.argv[1] + '-1' + '.eps\'\n')
    outPlotFile.write('set terminal postscript eps enhanced color\n')
    outPlotFile.write('set view map\n')
    outPlotFile.write('set xlabel \'var-decay\'\n')
    outPlotFile.write('set ylabel \'cla-decay\'\n')
    outPlotFile.write('plot \'' + sys.argv[1] + '.dat.1\' with image\n')
    outPlotFile.flush()
    outPlotFile.close()
    outPlotFile = open(sys.argv[1] + '.plot.2', 'w')
    outPlotFile.write('set out \'' + sys.argv[1] + '-2' + '.eps\'\n')
    outPlotFile.write('set terminal postscript eps enhanced color\n')
    outPlotFile.write('set view map\n')
    outPlotFile.write('set xlabel \'cla-decay\'\n')
    outPlotFile.write('set ylabel \'var-rand-freq\'\n')
    outPlotFile.write('plot \'' + sys.argv[1] + '.dat.2\' with image\n')
    outPlotFile.flush()
    outPlotFile.close()
    outPlotFile = open(sys.argv[1] + '.plot.3', 'w')
    outPlotFile.write('set out \'' + sys.argv[1] + '-3' + '.eps\'\n')
    outPlotFile.write('set terminal postscript eps enhanced color\n')
    outPlotFile.write('set view map\n')
    outPlotFile.write('set xlabel \'var-decay\'\n')
    outPlotFile.write('set ylabel \'var-rand-freq\'\n')
    outPlotFile.write('plot \'' + sys.argv[1] + '.dat.3\' with image\n')
    outPlotFile.flush()
    outPlotFile.close()
    outPlotFile = open(sys.argv[1] + '.opt.plot.1', 'w')
    outPlotFile.write('set out \'' + sys.argv[1] + '-1' + '.opt.eps\'\n')
    outPlotFile.write('set terminal postscript eps enhanced color\n')
    outPlotFile.write('set view map\n')
    outPlotFile.write('set xlabel \'var-decay\'\n')
    outPlotFile.write('set ylabel \'cla-decay\'\n')
    outPlotFile.write('plot \'' + sys.argv[1] + '.opt.dat.1\' with image\n')
    outPlotFile.flush()
    outPlotFile.close()
    outPlotFile = open(sys.argv[1] + '.opt.plot.2', 'w')
    outPlotFile.write('set out \'' + sys.argv[1] + '-2' + '.opt.eps\'\n')
    outPlotFile.write('set terminal postscript eps enhanced color\n')
    outPlotFile.write('set view map\n')
    outPlotFile.write('set xlabel \'cla-decay\'\n')
    outPlotFile.write('set ylabel \'var-rand-freq\'\n')
    outPlotFile.write('plot \'' + sys.argv[1] + '.opt.dat.2\' with image\n')
    outPlotFile.flush()
    outPlotFile.close()
    outPlotFile = open(sys.argv[1] + '.opt.plot.3', 'w')
    outPlotFile.write('set out \'' + sys.argv[1] + '-3' + '.opt.eps\'\n')
    outPlotFile.write('set terminal postscript eps enhanced color\n')
    outPlotFile.write('set view map\n')
    outPlotFile.write('set xlabel \'var-decay\'\n')
    outPlotFile.write('set ylabel \'var-rand-freq\'\n')
    outPlotFile.write('plot \'' + sys.argv[1] + '.opt.dat.3\' with image\n')
    outPlotFile.flush()
    outPlotFile.close()
    

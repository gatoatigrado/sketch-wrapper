#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: gatoatigrado (nicholas tung) [ntung at ntung], Sagar Jain [sagarj at cs.berkeley.edu]
# Copyright 2010 University of California, Berkeley

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain a
# copy of the License at http://www.apache.org/licenses/LICENSE-2.0 .

import re

class Plotter:
    def __init__(self, inFile, outFile):
        self.inFile = inFile    #Path to file containing the log of the sketch run
        self.outFile = outFile  #Path to outFile.dat containing rows of 'Iteration FindTime CheckTime' and outFile.plot file to use with gnuplot

    def plotTimeStatistics(self):
        '''Generates '($self.outFile).dat' containing rows of 'Iteration Number FindTime CheckTime', and '($self.outFile).plot' to be used with gnuplot. Note that sketch should be run with verbosity >= 5.'''
        inFile = open(self.inFile, 'r')
        decimalNumber = re.compile(r'(\d+\.?\d*)')
        timeLinePattern = "ftime= %s\tctime= %s" % (decimalNumber.pattern, decimalNumber.pattern)
        text = inFile.read()
        inFile.close()
        outDataFile = open(self.outFile + '.dat', 'w')
        outPlotFile = open(self.outFile + '.plot', 'w')
        timeLines = re.findall(timeLinePattern, text)
        for idx in xrange(len(timeLines)):
            outDataFile.write(str(idx) + "\t" + timeLines[idx][0] + "\t" + timeLines[idx][1] + "\n")
        outPlotFile.write('set out \'' + self.outFile + '.eps\'\n')
        outPlotFile.write('set terminal postscript eps enhanced color\n')
        outPlotFile.write('set xlabel \'Iteration Number\'\n')
        outPlotFile.write('set ylabel \'Time (ms)\'\n')
        outPlotFile.write('plot \'' + self.outFile + '.dat\' using 1:2 title \'Find-Time\' with lines, \'' + self.outFile + '.dat\' using 1:3 title \'Check-Time\' with lines')
        outPlotFile.flush()
        outDataFile.flush()
        outDataFile.close()
        outPlotFile.close()

    @staticmethod
    def plotComparisonsAcrossRuns(dataFileList, outFile):
        '''Input: $dataFileList is the list of the data files for various runs to be compared
Output: ($outFile).dat containing the combined information of timing statistics of input runs, and
        ($outFile).plot to be used for generating comparison plot using gnuplot'''
        outPlotFile = open(outFile + '.plot', 'w')
        outDataFile = open(outFile + '.dat', 'w')
        decimalNumber = re.compile(r'(\d+\.?\d*)')
        timePairs = []
        findCheckTimePairPattern = "\d\t%s\t%s" % (decimalNumber.pattern, decimalNumber.pattern)
        maxNumIters = -1
        for dataFile in dataFileList:
            inFile = open(dataFile, 'r')
            text = inFile.read()
            inFile.close()
            findCheckTimePairs = re.findall(findCheckTimePairPattern, text)
            maxNumIters = max(maxNumIters, len(findCheckTimePairs))
            timePairs.append(findCheckTimePairs)
        for iterNum in xrange(maxNumIters):
            lineToWrite = str(iterNum)
            for timePairsList in timePairs:
                if iterNum < len(timePairsList):
                    lineToWrite += '\t' + timePairsList[iterNum][0] + '\t' + timePairsList[iterNum][1]
                else:
                    lineToWrite += '\t' + ' ' + '\t' + ' '
            outDataFile.write(lineToWrite + '\n')
        outPlotFile.write('set out \'' + outFile + '.eps\'\n')
        outPlotFile.write('set terminal postscript eps enhanced color\n')
        outPlotFile.write('set xlabel \'Iteration Number\'\n')
        outPlotFile.write('set ylabel \'Time (ms)\'\n')
        plotLine = ''
        if len(dataFileList) > 0:
            plotLine += 'plot '
        for idx in xrange(len(dataFileList)):
            plotLine += '\'' + outFile + '.dat\' using 1:' + str(2 * (idx + 1)) + ' title \'Find-Time-Run-' + str(idx + 1) + '\' with lines, '
        for idx in xrange(len(dataFileList)):
            if idx != len(dataFileList) - 1:
                plotLine += '\'' + outFile + '.dat\' using 1:' + str(2 * (idx + 1) + 1) + ' title \'Check-Time-Run-' + str(idx + 1) + '\' with lines, '
            else:
                plotLine += '\'' + outFile + '.dat\' using 1:' + str(2 * (idx + 1) + 1) + ' title \'Check-Time-Run-' + str(idx + 1) + '\' with lines'
        outPlotFile.write(plotLine)
        outPlotFile.flush()
        outDataFile.flush()
        outPlotFile.close() 
        outDataFile.close()

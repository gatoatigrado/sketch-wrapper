#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: gatoatigrado (nicholas tung) [ntung at ntung], Sagar Jain [sagarj at cs.berkeley.edu]
# Copyright 2010 University of California, Berkeley

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain a
# copy of the License at http://www.apache.org/licenses/LICENSE-2.0 .

def estimate_free_ram():
    import subprocess
    try:
        text = subprocess.Popen(["free", "-b"], stdout=subprocess.PIPE).communicate()[0]
        line2 = text.split("\n")[2]
        assert line2.startswith("-/+")
        text = line2[(line2.index(":")+1):].strip().split()[1]
        return int(float(text) * 0.9)
    except OSError, e: 
        if e.errno != 2: raise
    try:
        import psutil
        return psutil.avail_phymem()
    except ImportError: pass

    import os
    if "SKETCH_RAM" in os.environ:
        return int(os.environ["SKETCH_RAM"])

    print("NOTE -- assuming 1GB of ram free. Set environment variable SKETCH_RAM (bytes), "
        "install psutil, or run Linux (and make sure the `free` utility is in your path)")
    return (1 << 30)

def set_rlimits():
    import resource
    bytes = estimate_free_ram()
    resource.setrlimit(resource.RLIMIT_AS, (bytes, bytes))

if __name__ == "__init__":
    set_rlimits()
